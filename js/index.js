var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
    })

    var myModalEl = document.getElementById('exampleModal')
    myModalEl.addEventListener('show.bs.modal', function (event) {
    // do something...
    console.log("El modal se esta mostrando");
        $("#btn-contact").removeClass("btn-primary");
        $("#btn-contact").addClass("btn-danger");
        $("#btn-contact").prop("disabled",true);
    })

    var myModalEl = document.getElementById('exampleModal')
    myModalEl.addEventListener('shown.bs.modal', function (event) {
    // do something...
    console.log("El modal se mostró");
    })

    var myModalEl = document.getElementById('exampleModal')
    myModalEl.addEventListener('hide.bs.modal', function (event) {
    // do something...
    console.log("El modal se oculta");
        $("#btn-contact").removeClass("btn-danger");
        $("#btn-contact").addClass("btn-primary");
        $("#btn-contact").prop("disabled",false);
    })

    var myModalEl = document.getElementById('exampleModal')
    myModalEl.addEventListener('hidden.bs.modal', function (event) {
    // do something...
    console.log("El modal se ocultó")
    })